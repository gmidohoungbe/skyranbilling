<?php

namespace App\Repository;

use App\Entity\DetailsProforma;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DetailsProforma|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetailsProforma|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetailsProforma[]    findAll()
 * @method DetailsProforma[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetailsProformaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetailsProforma::class);
    }

    // /**
    //  * @return DetailsProforma[] Returns an array of DetailsProforma objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DetailsProforma
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

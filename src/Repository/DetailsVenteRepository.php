<?php

namespace App\Repository;

use App\Entity\DetailsVente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DetailsVente|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetailsVente|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetailsVente[]    findAll()
 * @method DetailsVente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetailsVenteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetailsVente::class);
    }

    // /**
    //  * @return DetailsVente[] Returns an array of DetailsVente objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DetailsVente
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Service;

use App\Repository\ProduitRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PanierService
{

    protected $session;

    public function __construct(SessionInterface $session, ProduitRepository $produitRepository)
    {
        $this->session = $session;
        $this->produitRepository = $produitRepository;
    }

    public function ajouter(int $id)
    {
        # code...
        $panier = $this->session->get('panier', []);
        if (!empty($panier[$id])) {
        } else {
            $panier[$id] = 1;
        }
        $this->session->set('panier', $panier);
    }
    public function modifier(int $id, int $qte)
    {
        # code...
        $panier = $this->session->get('panier', []);
        
            $panier[$id] = $qte;
        
        $this->session->set('panier', $panier);
    }
    public function supprimer($id)
    {
        # code...
        $panier = $this->session->get('panier', []);
       
            unset($panier[$id]);
        
        $this->session->set('panier', $panier);
    }

    public function obtenirPanier(): array
    {
        # code...
        $panier = $this->session->get('panier', []);
        $donneesPanier = [];
        foreach ($panier as $id => $quantite) {
            $donneesPanier[] = [
                'produit' => $this->produitRepository->find($id),
                'quantite' => $quantite
            ];
        }
        return $donneesPanier;
    }

    public function obtenirProduitPanier($id): array
    {
        # code...
        $panier = $this->session->get('panier', []);
        $donneesPanier = [];
        
            $donneesPanier[] = [
                'produit' => $this->produitRepository->find($id),
                'quantite' => $panier[$id]
            ];
        
        return $donneesPanier;
    }

    public function viderPanier()
    {
        # code...
        $panier = $this->session->remove('panier');
      
    }

    // public function calculerTotal($i): float
    // {
    //     # code...
    // }
}

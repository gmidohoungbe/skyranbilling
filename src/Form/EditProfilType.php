<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class EditProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [               
                'required'  => false,
                'label'  => 'Nom',
                'attr'  => [
                    'class'  => 'form-control',
                    'placeholder'  => 'Entrez votre nom',

                ]
            ])
            ->add('password', PasswordType::class, [               
                'required'  => true,
                'label'  => 'Nouveau mot de passe',
                'attr'  => [
                    'class'  => 'form-control',
                    'placeholder'  => 'Entrez votre nouveau mot de passe',
                ]
            ])
            ->add('confirm_password', PasswordType::class, [               
                'required'  => true,
                'label'  => 'Confirmation',
                'attr'  => [
                    'class'  => 'form-control',
                    'placeholder'  => 'Retapez votre nouveau mot de passe',

                ]
            ])
            // ->add('email', EmailType::class, [
            //     'constraints'  =>[
            //         new NotBlank([
            //             'message'  => 'Merci de sasir une addresse email'
            //         ])
            //     ],
            //     'required'  => true,
            //     'attr'  => [
            //         'class'  => 'form-control'
            //     ]
            // ])
            ->add('Modifier', SubmitType::class, [
                'attr'  => [
                    'class'  => 'btn btn-success waves-effect waves-light mr-1'
                ]
            ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\Facture;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FactureFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero')
            ->add('date')
            ->add('totalHT')
            ->add('totalTTC')
            ->add('reste')
            ->add('montantPaye')
            ->add('cash')
            ->add('aib')
            ->add('mecefNim')
            ->add('mecefCompteur')
            ->add('mecefHeure')
            ->add('isf')
            ->add('codeqr')
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Facture::class,
        ]);
    }
}

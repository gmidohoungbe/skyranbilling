<?php

namespace App\Form;

use App\Entity\DetailsCommande;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class DetailsCommandeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantite')
            ->add('commande', EntityType::class, array('class' => 'App\Entity\Commande', 'choice_label' => 'id'))
            ->add('produit', EntityType::class, array('class' => 'App\Entity\Produit', 'choice_label' => 'nom'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DetailsCommande::class,
        ]);
    }
}

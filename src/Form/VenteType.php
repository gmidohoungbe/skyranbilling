<?php

namespace App\Form;

use App\Entity\Vente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
 
class VenteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateVente')
            ->add('totalHT')
            ->add('totalTTC')
            ->add('paye')
            ->add('client',EntityType::class, array('class' => 'App\Entity\Client','choice_label'=>'name' ))
            // ->add('facture',EntityType::class, array('class' => 'App\Entity\Facture','choice_label'=>'id' ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vente::class,
        ]);
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixHT;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixTTC;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailsVente", mappedBy="produit")
     */
    private $detailsVentes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailsCommande", mappedBy="produit")
     */
    private $detailsCommandes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailsProforma", mappedBy="produit")
     */
    private $detailsProformas;

    public function __construct()
    {
        $this->detailsVentes = new ArrayCollection();
        $this->detailsCommandes = new ArrayCollection();
        $this->detailsProformas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrixHT(): ?int
    {
        return $this->prixHT;
    }

    public function setPrixHT(int $prixHT): self
    {
        $this->prixHT = $prixHT;

        return $this;
    }

    public function getPrixTTC(): ?int
    {
        return $this->prixTTC;
    }

    public function setPrixTTC(int $prixTTC): self
    {
        $this->prixTTC = $prixTTC;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return Collection|DetailsVente[]
     */
    public function getDetailsVentes(): Collection
    {
        return $this->detailsVentes;
    }

    public function addDetailsVente(DetailsVente $detailsVente): self
    {
        if (!$this->detailsVentes->contains($detailsVente)) {
            $this->detailsVentes[] = $detailsVente;
            $detailsVente->setProduit($this);
        }

        return $this;
    }

    public function removeDetailsVente(DetailsVente $detailsVente): self
    {
        if ($this->detailsVentes->contains($detailsVente)) {
            $this->detailsVentes->removeElement($detailsVente);
            // set the owning side to null (unless already changed)
            if ($detailsVente->getProduit() === $this) {
                $detailsVente->setProduit(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DetailsCommande[]
     */
    public function getDetailsCommandes(): Collection
    {
        return $this->detailsCommandes;
    }

    public function addDetailsCommande(DetailsCommande $detailsCommande): self
    {
        if (!$this->detailsCommandes->contains($detailsCommande)) {
            $this->detailsCommandes[] = $detailsCommande;
            $detailsCommande->setProduit($this);
        }

        return $this;
    }

    public function removeDetailsCommande(DetailsCommande $detailsCommande): self
    {
        if ($this->detailsCommandes->contains($detailsCommande)) {
            $this->detailsCommandes->removeElement($detailsCommande);
            // set the owning side to null (unless already changed)
            if ($detailsCommande->getProduit() === $this) {
                $detailsCommande->setProduit(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DetailsProforma[]
     */
    public function getDetailsProformas(): Collection
    {
        return $this->detailsProformas;
    }

    public function addDetailsProforma(DetailsProforma $detailsProforma): self
    {
        if (!$this->detailsProformas->contains($detailsProforma)) {
            $this->detailsProformas[] = $detailsProforma;
            $detailsProforma->setProduit($this);
        }

        return $this;
    }

    public function removeDetailsProforma(DetailsProforma $detailsProforma): self
    {
        if ($this->detailsProformas->contains($detailsProforma)) {
            $this->detailsProformas->removeElement($detailsProforma);
            // set the owning side to null (unless already changed)
            if ($detailsProforma->getProduit() === $this) {
                $detailsProforma->setProduit(null);
            }
        }

        return $this;
    }


}

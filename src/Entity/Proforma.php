<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProformaRepository")
 */
class Proforma
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePro;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="proformas")
     */
    private $Client;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailsProforma", mappedBy="proforma")
     */
    private $detailsProformas;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalHT;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalTTC;

    public function __construct()
    {
        $this->detailsProformas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatePro(): ?\DateTimeInterface
    {
        return $this->datePro;
    }

    public function setDatePro(\DateTimeInterface $datePro): self
    {
        $this->datePro = $datePro;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->Client;
    }

    public function setClient(?Client $Client): self
    {
        $this->Client = $Client;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|DetailsProforma[]
     */
    public function getDetailsProformas(): Collection
    {
        return $this->detailsProformas;
    }

    public function addDetailsProforma(DetailsProforma $detailsProforma): self
    {
        if (!$this->detailsProformas->contains($detailsProforma)) {
            $this->detailsProformas[] = $detailsProforma;
            $detailsProforma->setProforma($this);
        }

        return $this;
    }

    public function removeDetailsProforma(DetailsProforma $detailsProforma): self
    {
        if ($this->detailsProformas->contains($detailsProforma)) {
            $this->detailsProformas->removeElement($detailsProforma);
            // set the owning side to null (unless already changed)
            if ($detailsProforma->getProforma() === $this) {
                $detailsProforma->setProforma(null);
            }
        }

        return $this;
    }

    public function getTotalHT(): ?int
    {
        return $this->totalHT;
    }

    public function setTotalHT(?int $totalHT): self
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    public function getTotalTTC(): ?int
    {
        return $this->totalTTC;
    }

    public function setTotalTTC(?int $totalTTC): self
    {
        $this->totalTTC = $totalTTC;

        return $this;
    }
}

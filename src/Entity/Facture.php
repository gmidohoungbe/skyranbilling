<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\FactureRepository")
 * @UniqueEntity(fields="numero", message="Une facture exite déjà avec ce titre.")
 */
class Facture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $numero;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero
     */
    private $totalHT;

    /**
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero
     */
    private $totalTTC;

    /**
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero
     */
    private $reste;

    /**
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero
     */
    private $montantPaye;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cash;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Paiement", mappedBy="facture")
     */
    private $paiements;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Vente", inversedBy="facture", cascade={"persist", "remove"})
     */
    private $vente;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Intervention", inversedBy="facture", cascade={"persist", "remove"})
     */
    private $intervention;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Commande", inversedBy="facture", cascade={"persist", "remove"})
     */
    private $commande;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $aib;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mecefNim;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mecefCompteur;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $mecefHeure;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $isf;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $codeqr;

    public function __construct()
    {
        $this->paiements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTotalHT(): ?int
    {
        return $this->totalHT;
    }

    public function setTotalHT(int $totalHT): self
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    public function getTotalTTC(): ?int
    {
        return $this->totalTTC;
    }

    public function setTotalTTC(int $totalTTC): self
    {
        $this->totalTTC = $totalTTC;

        return $this;
    }

    public function getReste(): ?int
    {
        return $this->reste;
    }

    public function setReste(int $reste): self
    {
        $this->reste = $reste;

        return $this;
    }

    public function getMontantPaye(): ?int
    {
        return $this->montantPaye;
    }

    public function setMontantPaye(int $montantPaye): self
    {
        $this->montantPaye = $montantPaye;

        return $this;
    }

    public function getCash(): ?bool
    {
        return $this->cash;
    }

    public function setCash(bool $cash): self
    {
        $this->cash = $cash;

        return $this;
    }

    /**
     * @return Collection|Paiement[]
     */
    public function getPaiements(): Collection
    {
        return $this->paiements;
    }

    public function addPaiement(Paiement $paiement): self
    {
        if (!$this->paiements->contains($paiement)) {
            $this->paiements[] = $paiement;
            $paiement->setFacture($this);
        }

        return $this;
    }

    public function removePaiement(Paiement $paiement): self
    {
        if ($this->paiements->contains($paiement)) {
            $this->paiements->removeElement($paiement);
            // set the owning side to null (unless already changed)
            if ($paiement->getFacture() === $this) {
                $paiement->setFacture(null);
            }
        }

        return $this;
    }

    public function getVente(): ?Vente
    {
        return $this->vente;
    }

    public function setVente(?Vente $vente): self
    {
        $this->vente = $vente;

        return $this;
    }

    public function getIntervention(): ?Intervention
    {
        return $this->intervention;
    }

    public function setIntervention(?Intervention $intervention): self
    {
        $this->intervention = $intervention;

        return $this;
    }

    public function getCommande(): ?Commande
    {
        return $this->commande;
    }

    public function setCommande(?Commande $commande): self
    {
        $this->commande = $commande;

        return $this;
    }

    public function getAib(): ?float
    {
        return $this->aib;
    }

    public function setAib(?float $aib): self
    {
        $this->aib = $aib;

        return $this;
    }

    public function getMecefNim(): ?string
    {
        return $this->mecefNim;
    }

    public function setMecefNim(?string $mecefNim): self
    {
        $this->mecefNim = $mecefNim;

        return $this;
    }

    public function getMecefCompteur(): ?string
    {
        return $this->mecefCompteur;
    }

    public function setMecefCompteur(?string $mecefCompteur): self
    {
        $this->mecefCompteur = $mecefCompteur;

        return $this;
    }

    public function getMecefHeure(): ?\DateTimeInterface
    {
        return $this->mecefHeure;
    }

    public function setMecefHeure(?\DateTimeInterface $mecefHeure): self
    {
        $this->mecefHeure = $mecefHeure;

        return $this;
    }

    public function getIsf(): ?string
    {
        return $this->isf;
    }

    public function setIsf(?string $isf): self
    {
        $this->isf = $isf;

        return $this;
    }

    public function getCodeqr(): ?string
    {
        return $this->codeqr;
    }

    public function setCodeqr(?string $codeqr): self
    {
        $this->codeqr = $codeqr;

        return $this;
    }
}

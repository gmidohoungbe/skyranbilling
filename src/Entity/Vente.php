<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VenteRepository")
 */
class Vente
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateVente;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalHT;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalTTC;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paye;

    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="vente")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Facture", mappedBy="vente", cascade={"persist", "remove"})
     */
    private $facture;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailsVente", mappedBy="vente")
     */
    private $detailsVentes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numeroVente;

    public function __construct()
    {
        $this->detailsVentes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateVente(): ?\DateTimeInterface
    {
        return $this->dateVente;
    }

    public function setDateVente(\DateTimeInterface $dateVente): self
    {
        $this->dateVente = $dateVente;

        return $this;
    }

    public function getTotalHT(): ?int
    {
        return $this->totalHT;
    }

    public function setTotalHT(int $totalHT): self
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    public function getTotalTTC(): ?int
    {
        return $this->totalTTC;
    }

    public function setTotalTTC(int $totalTTC): self
    {
        $this->totalTTC = $totalTTC;

        return $this;
    }

    public function getPaye(): ?bool
    {
        return $this->paye;
    }

    public function setPaye(bool $paye): self
    {
        $this->paye = $paye;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getFacture(): ?Facture
    {
        return $this->facture;
    }

    public function setFacture(?Facture $facture): self
    {
        $this->facture = $facture;

        // set (or unset) the owning side of the relation if necessary
        $newVente = null === $facture ? null : $this;
        if ($facture->getVente() !== $newVente) {
            $facture->setVente($newVente);
        }

        return $this;
    }

    /**
     * @return Collection|DetailsVente[]
     */
    public function getDetailsVentes(): Collection
    {
        return $this->detailsVentes;
    }

    public function addDetailsVente(DetailsVente $detailsVente): self
    {
        if (!$this->detailsVentes->contains($detailsVente)) {
            $this->detailsVentes[] = $detailsVente;
            $detailsVente->setVente($this);
        }

        return $this;
    }

    public function removeDetailsVente(DetailsVente $detailsVente): self
    {
        if ($this->detailsVentes->contains($detailsVente)) {
            $this->detailsVentes->removeElement($detailsVente);
            // set the owning side to null (unless already changed)
            if ($detailsVente->getVente() === $this) {
                $detailsVente->setVente(null);
            }
        }

        return $this;
    }

    public function getNumeroVente(): ?string
    {
        return $this->numeroVente;
    }

    public function setNumeroVente(string $numeroVente): self
    {
        $this->numeroVente = $numeroVente;

        return $this;
    }
}

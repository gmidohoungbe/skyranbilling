<?php

namespace App\Controller;

use App\Entity\Vente;
use App\Entity\Client;
use App\Entity\Produit;
use App\Form\VenteType;
use App\Entity\DetailsVente;
use App\Service\PanierService;
use App\Form\Produit\ProduitType;
use App\Service\SerializerService;
use App\Repository\VenteRepository;
use App\Repository\ClientRepository;
use App\Repository\ProduitRepository;
use App\Repository\DetailsVenteRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class VenteController extends AbstractController
{
    protected $ht = 0;
    protected $ttc = 0;
    /**
     * @Route("/vente", name="vente")
     */
    public function index()
    {
        return $this->render('vente/index.html.twig', [
            'controller_name' => 'VenteController',
        ]);
    }



    //----------------------FONCTION POUR AFFICHER LA LISTE DES PRODUITS--------------------------

    /**
     * @Route("vente/ajoutVente", options ={ "expose" = true},name="ajout_vente")
     */
    public function ajouterVente(Vente $vente = null, ProduitRepository $repo)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            $produits = $repo->findInStock();

            $vente = new Vente();

            $form = $this->createForm(VenteType::class, $vente);

            return $this->render('vente/ajoutVente.html.twig', [
                'produits' => $produits,
                'controller_name' => 'VenteController', 'formVente' => $form->createView()
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }


    //-----------------------FONCTION POUR AJOUTER AU PANIER--------------------------------------

    /**
     * @Route("/vente/panier/{id}",options ={ "expose" = true}, name="ajout_panier")
     * 
     */
    public function ajoutPanier($id, PanierService $monpanier)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            // $repo= $this->getDoctrine()->getRepository(Produit::class);

            // $produits = $repo->findAll();
            // foreach ($produits as $idprod => $prod) {
            //   if($produits->getStock()>0)
            //   {
            $monpanier->ajouter($id);
            //   }
            //   else
            //   {
            //     $this->addFlash(
            //       'Note',
            //     'Aucun stock disponible'
            //     );
            //   }
            // }     

            return $this->json([
                'message' => "Produit ajouté au panier"
            ], 200);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    //---------------------------------------FONCTION POUR VOIR LE PANIER--------------------------

    /**
     * @Route("/vente/voir/panier",options ={ "expose" = true}, name="voir_panier")
     * 
     */

    public function voirPanier(PanierService $monpanier): Response
    {

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);
        $listePanier = $monpanier->obtenirPanier();

        $jsonContent = $serializer->serialize($listePanier, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['client', 'facture', 'detailsCommandes', 'livraisons', 'detailsVentes', 'detailsProformas']]);

        return $this->json([
            $jsonContent
        ], 200);
    }


    //---------------------------------FONCTION POUR MODIFIER LE PANIER-----------------------------

    /**
     * @Route("/vente/modifier/panier/{id}/{qte}",options ={ "expose" = true}, name="modifier_panier")
     * 
     */
    public function modifierPanier($id, $qte, PanierService $monpanier)
    {
        $monpanier->modifier($id, $qte);
        $listePanier = $monpanier->obtenirProduitPanier($id);
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($listePanier, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['client', 'facture', 'detailsCommandes', 'livraisons', 'detailsVentes', 'detailsProformas']]);
        return $this->json([
            $jsonContent
        ], 200);
    }


    //--------------------------------------FONCTION POUR RETIRER DU PANIER--------------------------

    /**
     * @Route("/vente/retirer/panier/{id}", name="retirer_panier")
     */
    public function supprimerPanier($id, PanierService $monpanier): Response
    {

        $monpanier->supprimer($id);
        return $this->json(['message' => "Produit supprimé du panier"], 200);
    }


    //--------------------------------------ANNULER LA VENTE--------------------------------------
    /**
     * @Route("/vente/annuler",options ={ "expose" = true}, name="annuler_vente")
     */
    public function annulerVente(PanierService $monpanier)
    {
        $monpanier->viderPanier();
        return $this->json(['message' => "Opération annulée"], 200);
    }


    //---------------------------------------VALIDER LA VENTE---------------------------------------
    /**
     * @Route("/vente/valider/{client}/{HT}/{TTC}",options ={ "expose" = true}, name="valider_vente")
     *
     */
    public function validerVente($client, $HT, $TTC, ClientRepository $clients, DetailsVenteRepository $detailVenteRepo, PanierService $monpanier, ProduitRepository $produits, VenteRepository $venteRepository)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            // Création de la vente
            $client = $clients->findOneById($client);
            $vente = new Vente();
            $vente->setNumeroVente($this->genererNumVente($venteRepository));
            $vente->setClient($client);
            $vente->setPaye(1);
            $vente->setDateVente(new \DateTime());
            $vente->setTotalHT($HT);
            $vente->setTotalTTC($TTC);
            // $vente -> setPaye($paye);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($vente);
            $manager->flush();

            // Création des détails de la vente
            $listePanier = $monpanier->obtenirPanier();

            foreach ($listePanier as $idprod => $item) {

                $detVente = new DetailsVente();

                $detVente->setVente($vente);
                $prod = $produits->findOneById($item['produit']->getId());
                $detVente->setProduit($prod);
                $detVente->setQuantite($item['quantite']);

                $stock = $prod->getStock();
                $prod->setStock($stock - $item['quantite']);

                $manager = $this->getDoctrine()->getManager();
                $manager->persist($detVente);
                $manager->flush();

                $manager->persist($prod);
                $manager->flush();
            }
            $monpanier->viderPanier();

            return $this->redirectToRoute('liste_vente');
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * @Route("/vente/listeVente",options = {"expose" = true}, name="liste_vente")
     *
     */
    public function listerVente(VenteRepository $ventesRepository)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $ventes = $ventesRepository->findAll();

            return $this->render('vente/listeVente.html.twig', [
                'ventes' => $ventes
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * @Route("/vente/detailsVente/{id}", name="details_vente")
     *
     */
    public function detailsVente(Vente $vente, DetailsVenteRepository $detventesRepository)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            return $this->render('vente/detailsVente.html.twig', [
                'vente' => $vente
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * @Route("vente/modifierVente", name="modifier_vente")
     */
    public function modifierVente(VenteRepository $ventesRepository)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $ventes = $ventesRepository
                ->findAll();

            return $this->render('vente/listeVente.html.twig', [
                'ventes' => $ventes
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
    /**
     * @Route("vente/supprimerVente", name="supprimer_vente")
     */
    public function supprimerVente(VenteRepository $ventesRepository)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            $ventes = $ventesRepository
                ->findAll();

            return $this->render('vente/listeVente.html.twig', [
                'ventes' => $ventes
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * @Route("vente/genererNum", name="vente_number")
     
     */
    public function genererNumVente(VenteRepository $venteRepository):String
    {

        $nb = $venteRepository->countVente();

        $val = (string) $nb + 1;

        $num = ('Vent-' . $val);

        return $num;
    }
}

<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Entity\Commande;
use App\Form\CommandeType;
use App\Service\PanierService;
use App\Entity\DetailsCommande;
use App\Repository\ClientRepository;
use App\Repository\ProduitRepository;
use App\Repository\CommandeRepository;
use Symfony\Component\Serializer\Serializer;
use App\Repository\DetailsCommandeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class CommandeController extends AbstractController
{

    /**
     * @Route("commande/genererNum", name="commande_number")
     */
    public function genererNumCmd(CommandeRepository $commandesRepository): String
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            $nb = $commandesRepository->countCommande();

            $val = (string) $nb + 1;

            $num = ('Cmd-' . $val);

            return new $num;
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
        return '';
    } 

    /**
     * @Route("/ajout/commande", 
     * options ={ "expose" = true},
     * name="ajout_commande")
     */
    public function ajouterCommande(Request $request, ProduitRepository $produits)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $commande = new Commande();

            $listProduits = $produits
                ->findInStock();
            $form = $this->createForm(CommandeType::class, $commande);
            $form->handleRequest($request);
            return $this->render('commande/ajoutCmd.html.twig', [
                'formCommande' => $form->createView(),
                'produits' => $listProduits
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    //création de la fonction qui gère l'ajout des produits
    /**
     * @Route("/commande/panier/{id}",
     * options ={ "expose" = true},
     *  name="ajout_panier"
     * )
     */
    public function addPanier($id, PanierService $panierService)
    {
        $panierService->ajouter($id);

        // $listePanier = $panierService->obtenirPanier();
        return $this->json([
            'message' => "Produit ajouté au panier"
            //,'panier' => $listePanier
        ], 200);
    }

    //création de la fonction qui gère l'ajout des produits
    /**
     * @Route("/commande/modifpanier/{id}/{qte}",
     * options ={ "expose" = true},
     *  name="modifier_panier"
     * )
     */
    public function modifierPanier($id, $qte, PanierService $panierService)
    {
        $panierService->modifier($id, $qte);
        $listePanier = $panierService->obtenirProduitPanier($id);
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($listePanier, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['client', 'facture', 'detailsCommandes', 'livraisons', 'detailsVentes', 'detailsProformas']]);
        return $this->json([
            $jsonContent
        ], 200);
    }

    //création de la fonction qui gère la suppresion des produits
    /**
     * @Route("/commande/panier/suppr/{id}", 
     * options ={ "expose" = true},
     * name="supprimer_panier")
     */
    public function supprimerPanier($id, PanierService $panierService): Response
    {

        $panierService->supprimer($id);
        return $this->json(['message' => "Produit supprimé du panier"], 200);
    }

    //création de la fonction qui gère l'ajout des produits
    /**
     * @Route("/commande/monpanier/obtenirpanier/",
     * options = {"expose" = true}, 
     * name="obtenir_panier")
     */
    public function obtenirPanier(PanierService $panierService): Response
    {

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $listePanier = $panierService->obtenirPanier();
        $jsonContent = $serializer->serialize($listePanier, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['client', 'facture', 'detailsCommandes', 'livraisons', 'detailsVentes', 'detailsProformas']]);
        return $this->json([
            $jsonContent
        ], 200);
    }

    // Annulation de commande
    /**
     * @Route("/commande/annuler/",
     * options = {"expose" = true}, 
     * name="annuler_commande")
     */
    public function annulerCommande(PanierService $panierService)
    {
        $panierService->viderPanier();
        return $this->json(['message' => "Opération annulée"], 200);
    }

    /**
     * @Route("/commande/editDetailsCmd/{id}", name="edit_commande")
     */
    public function editDetailsCommande(Commande $cmd, Request $request, DetailsCommandeRepository $detailCmdRepo)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {



            return $this->render('commande/editCommande.html.twig', [
                'commande' => $cmd
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }


    /**
     * @Route("/commande/valider/{client}/{total}",
     * options = {"expose" = true}, 
     *  name="valider_commande")
     */
    public function ValiderCommande($client, $total, ClientRepository $clients, Request $request, DetailsCommandeRepository $detailCmdRepo, PanierService $panierService, ProduitRepository $produits,CommandeRepository $commandeRepository)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            // Création de la commande

            $client = $clients->findOneById($client);
            $commande = new Commande();
            $commande->setClient($client);
            $commande->setDateCmd(new \DateTime());

            $commande->setNumeroCmd($this->genererNumCmd($commandeRepository));
            $commande->setTotalHT($total);
            $ttc = $total * 1.18;
            $commande->setTotalTTC($ttc);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($commande);
            $manager->flush();

            // Création des détails de la commande
            $listePanier = $panierService->obtenirPanier();
            foreach ($listePanier as $idprod => $item) {

                $detailscmd = new DetailsCommande();
                $detailscmd->setCommande($commande);
                $prod = $produits->findOneById($item['produit']->getId());
                $detailscmd->setProduit($prod);
                $prod->setStock($prod->getStock() - $item['quantite']);
                $detailscmd->setQuantite($item['quantite']);
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($detailscmd);
                $manager->flush();
                $manager->persist($prod);
                $manager->flush();
            }
            $panierService->viderPanier();

            return $this->redirectToRoute('liste_commande');
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }



    /**
     * @Route("/commande/listeCommande",
     *  options = {"expose" = true}, 
     *  name="liste_commande")
     */
    public function listerCommande(CommandeRepository $commandesRepository)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $commandes = $commandesRepository
                ->findAll();

            return $this->render('commande/listeCommande.html.twig', [
                'commandes' => $commandes
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
    /**
     * @Route("/modifier/commande", name="modifier_commande")
     */
    public function modifierCommande(CommandeRepository $commandesRepository)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            $commandes = $commandesRepository
                ->findAll();

            return $this->render('commande/listeCommande.html.twig', [
                'commandes' => $commandes
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
    /**
     * @Route("/supprimer/commande", name="supprimer_commande")
     */
    public function supprimerCommande(CommandeRepository $commandesRepository)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $commandes = $commandesRepository
                ->findAll();

            return $this->render('commande/listeCommande.html.twig', [
                'commandes' => $commandes
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
}

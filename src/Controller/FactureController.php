<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Facture;
use App\Entity\Commande;
use App\Form\FactureType;
use App\Form\FactureFormType;
use App\Entity\DetailsCommande;
use App\Repository\VenteRepository;
use App\Repository\ClientRepository;
use App\Repository\FactureRepository;
use App\Repository\ProduitRepository;
use App\Repository\CommandeRepository;
use App\Repository\DetailsVenteRepository;
use App\Repository\InterventionRepository;
use App\Repository\DetailsCommandeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class FactureController extends AbstractController
{
    /**
     * @Route("/facture", name="facture")
     */
    public function index()
    {
        return $this->render('facture/index.html.twig', [
            'controller_name' => 'FactureController',
        ]);
    }

    /**
     * @Route("facture/genererFact", name="facture_number")
     */
    public function genererNumFact(): String
    {

        $nb = $this->getDoctrine()->getRepository(Facture::class)->countFacture();

        $val = (string) $nb + 1;

        $num = ('Fact-' . $val);

        return $num;
    }

    /**
     * @Route("/listefacture", name="liste_facture")
     */
    public function listerFacture(FactureRepository $factureRepository)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $facture = $factureRepository->findAll();
            return $this->render('facture/listeFacture.html.twig', [
                'factures' => $facture,
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * @Route("/vuefacture", name="vue_facture")
     */
    public function visualiserFacture()
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            return $this->render('facture/vueFacture.html.twig', [
                'controller_name' => 'FactureController',
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * @Route("facture/vuefacture/{id}/{cas}", name="vue_Cmdfacture")
     */
    public function visualiserCmdFacture($id, $cas, ClientRepository $clientRepository, FactureRepository $factureRepository, CommandeRepository $commandeRepository, DetailsCommandeRepository $detailsCommandeRepository, VenteRepository $venteRepository, DetailsVenteRepository $detailsVenteRepository)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            if ($cas == 0) {

                $facture = new Facture();
                $facture = $factureRepository->findOneByCommande($id);

                // Récupération de la commande
                $commande = $commandeRepository->findOneById($id);
                $detailsCmd = $detailsCommandeRepository->findByCommande($commande->getId());
                $client = new Client();
                $client = $clientRepository->findOneById($commande->getClient());

                return $this->render('facture/vueFacture.html.twig', [
                    'client' => $client->getName(),
                    'objet' => 'Commande ' . $commande->getNumeroCmd(),
                    'detailsCmd' => $detailsCmd,
                    'facture' => $facture

                ]);
            } elseif ($cas == 1) {
                $facture = new Facture();
                $facture = $factureRepository->findOneByVente($id);

                // Récupération de la vente
                $vente = $venteRepository->findOneById($id);
                $detailsVente = $detailsVenteRepository->findByVente($vente->getId());
                //$totalHT = $commande->getTotalHT();
                //$totalTTC = $commande->getTotalTTC();

                $client = new Client();
                $client = $clientRepository->findOneById($vente->getClient());

                return $this->render('facture/vueFacture.html.twig', [
                    'client' => $client->getName(),
                    'objet' => 'Vente ID : ' . $facture->getVente()->getNumeroVente(),
                    'detailsCmd' => $detailsVente,
                    'facture' => $facture

                ]);
            }
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
    /**
     * @Route("/ajouterFacture/{id}/{fac}", name="ajout_facture")
     */
    public function ajoutFacture(Request $request, $id, $fac, CommandeRepository $commandeRepository, VenteRepository $venteRepository, ClientRepository $clientRepository, DetailsCommandeRepository $detailsCommandeRepository, DetailsVenteRepository $detailsVenteRepository, ProduitRepository $produitRepository, InterventionRepository $interventionRepository)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            if ($fac == 0) {
                // Calcul du montant total,
                //$total= 0;
                $client = new Client();
                //$commande = new Commande();
                $commande = $commandeRepository->findOneById($id);
                $totalHT = $commande->getTotalHT();
                $totalTTC = $commande->getTotalTTC();
                //$detailsCmd = $detailsCommandeRepository-> findOneByCommande($commande->getId());
                $detailsCmd = $detailsCommandeRepository->findByCommande($commande->getId());
                //foreach ($detailsCmd as $detail) {
                # code...
                // $produit = $detail->getProduit();
                //$prix = $produitRepository->findOneById($produit)->getPrixHt() * $detail->getQuantite();
                //$total = $total + $prix;
                //}
                //$totalTTC= $total *1.18;
                $client = $clientRepository->findOneById($commande->getClient());
                $facture = new Facture();
                $facture->setNumero($this->genererNumFact());
                $form = $this->createForm(FactureType::class, $facture);

                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {


                    $facture->setDate(new \DateTime());
                    $facture->setCommande($commande);
                    $facture->setTotalHT($totalHT);
                    $facture->setTotalTTC($totalTTC);
                    $montantPaye = $facture->getMontantPaye();
                    if ($totalTTC > $montantPaye) {
                        $facture->setReste($totalTTC - $montantPaye);
                    }else {
                        # code...
                        $facture->setReste(0);
                    }
                    
                    //$facture-> setMecefHeure(new \DateTime());

                    $manager = $this->getDoctrine()->getManager();
                    $manager->persist($facture);
                    $manager->flush();
                    $tva = $totalHT * 0.18;
                    return $this->render('facture/vueFacture.html.twig', [
                        'client' => $client->getName(),
                        'totalHT' => $totalHT,
                        'id' => $id,
                        'mecefNim' =>  $facture->getMecefNim(),
                        'mecefCompteur' => $facture->getMecefCompteur(),
                        'mecefHeure' => $facture->getMecefHeure(),
                        'isf' => $facture->getIsf(),
                        'codeqr' => $facture->getCodeqr(),
                        'numero' =>  $facture->getNumero(),
                        'montantpayé' => $facture->getMontantPaye(),
                        'objet' => 'Commande avec ID N°' . $commande->getNumeroCmd(),
                        'totalTTC' => $totalTTC,
                        'tva' => $tva,
                        'detailsCmd' => $detailsCmd,
                        'facture' => $facture

                    ]);
                }
                $commande = $commandeRepository->findOneById($id);
                //$client->$commande->getClient();

                return $this->render('facture/ajoutFacture.html.twig', [
                    'formFacture' => $form->createView(),
                    'client' => $client,
                    'detail' => $detailsCmd,
                    'totalHT' => $totalHT,
                    'totalTTC' => $totalTTC
                ]);
            } elseif ($fac == 1) {

                // Calcul du montant total,
                //$total= 0;
                $client = new Client();
                //$commande = new Commande();
                $vente = $venteRepository->findOneById($id);
                $totalHT = $vente->getTotalHT();
                $totalTTC = $vente->getTotalTTC();
                //$detailsCmd = $detailsCommandeRepository-> findOneByCommande($commande->getId());
                $detailsVente = $detailsVenteRepository->findByVente($vente->getId());
                //foreach ($detailsCmd as $detail) {
                # code...
                // $produit = $detail->getProduit();
                //$prix = $produitRepository->findOneById($produit)->getPrixHt() * $detail->getQuantite();
                //$total = $total + $prix;
                //}
                //$totalTTC= $total *1.18;
                $client = $clientRepository->findOneById($vente->getClient());
                $facture = new Facture();
                $form = $this->createForm(FactureType::class, $facture);
                $facture->setNumero($this->genererNumFact());
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {

                    $facture->setDate(new \DateTime());

                    $facture->setVente($vente);
                    $facture->setTotalHT($totalHT);
                    $facture->setTotalTTC($totalTTC);
                    $montantPaye = $facture->getMontantPaye();
                    if ($totalTTC > $montantPaye) {
                        $facture->setReste($totalTTC - $montantPaye);
                    } else {
                        # code...
                        $facture->setReste(0);
                    }
                    //$facture->setMecefHeure(new \DateTime());
                    $manager = $this->getDoctrine()->getManager();
                    $manager->persist($facture);
                    $manager->flush();
                    $tva = $totalHT * 0.18;
                    return $this->render('facture/vueFacture.html.twig', [
                        'client' => $client->getName(),
                        'totalHT' => $totalHT,
                        'id' => $id,
                        'mecefNim' =>  $facture->getMecefNim(),
                        'mecefCompteur' => $facture->getMecefCompteur(),
                        'mecefHeure' => $facture->getMecefHeure(),
                        'isf' => $facture->getIsf(),
                        'numero' =>  $facture->getNumero(),
                        'montantpayé' => $facture->getMontantPaye(),
                        'objet' => 'Vente avec ID N°' . $vente->getId(),
                        'totalTTC' => $totalTTC,
                        'tva' => $tva,
                        'detailsCmd' => $detailsVente,
                        'facture' => $facture

                    ]);
                }
                $vente = $venteRepository->findOneById($id);
                //$client->$commande->getClient();

                return $this->render('facture/ajoutFacture.html.twig', [
                    'formFacture' => $form->createView(),
                    'client' => $client,
                    'detail' => $detailsVente,
                    'totalHT' => $totalHT,
                    'totalTTC' => $totalTTC
                ]);
            } elseif ($fac == 2) {


                $client = new Client();
                $intervention = $interventionRepository->findOneById($id);
                //$totalHT = $vente->getTotalHT();
                //$totalTTC = $vente->getTotalTTC();
                //$detailsCmd = $detailsCommandeRepository-> findOneByCommande($commande->getId());
                //$detailsVente = $detailsVenteRepository->findByVente($vente->getId());
                //foreach ($detailsCmd as $detail) {
                # code...
                // $produit = $detail->getProduit();
                //$prix = $produitRepository->findOneById($produit)->getPrixHt() * $detail->getQuantite();
                //$total = $total + $prix;
                //}
                //$totalTTC= $total *1.18;
                $client = $clientRepository->findOneById($intervention->getClient());
                $facture = new Facture();
                $facture->setNumero($this->genererNumFact());
                $form = $this->createForm(FactureType::class, $facture);

                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {

                    $facture->setDate(new \DateTime());

                    $facture->setintervention($intervention);
                    $facture->setTotalHT($intervention->getTotalHT());
                    $facture->setTotalTTC($intervention->getTotalTTC());
                    $montantPaye = $facture->getMontantPaye();
                    if ($totalTTC > $montantPaye) {
                        $facture->setReste($totalTTC - $montantPaye);
                    } else {
                        # code...
                        $facture->setReste(0);
                    }
                    //$facture->setMecefHeure(new \DateTime());
                    $manager = $this->getDoctrine()->getManager();
                    $manager->persist($facture);
                    $manager->flush();
                    $tva = $totalHT * 0.18;
                    return $this->render('facture/vueFactureIntervention.html.twig', [
                        'client' => $client->getName(),
                        'totalHT' => $totalHT,
                        'id' => $id,
                        'mecefNim' =>  $facture->getMecefNim(),
                        'mecefCompteur' => $facture->getMecefCompteur(),
                        'mecefHeure' => $facture->getMecefHeure(),
                        'isf' => $facture->getIsf(),
                        'numero' =>  $facture->getNumero(),
                        'montantpayé' => $facture->getMontantPaye(),
                        'objet' => 'Intervention avec ID N°' . $intervention->getNumero(),
                        'totalTTC' => $totalTTC,
                        'tva' => $tva,
                        'facture' => $facture

                    ]);
                }
                $vente = $venteRepository->findOneById($id);
                //$client->$commande->getClient();

                return $this->render('facture/ajoutFacture.html.twig', [
                    'formFacture' => $form->createView(),
                    'client' => $client,
                    'totalHT' => $facture->getTotalHT(),
                    'totalTTC' => $facture->gettotalTTC()
                ]);
            }
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
    /**
     * @Route("/editerCmdFacture/{id}", name="edit_cmdFacture")
     */
    public function editerCmdFacture($id, FactureRepository $factureRepository, Request $request)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $facture = $factureRepository->findOneById($id);
            $form = $this->createForm(FactureFormType::class, $facture);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $manager = $this->getDoctrine()->getManager();
                $manager->persist($facture);
                $manager->flush();

                return $this->redirectToRoute('liste_facture');
            }
            return $this->render('facture/editFacture.html.twig', [
                'formFacture' => $form->createView()
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
}

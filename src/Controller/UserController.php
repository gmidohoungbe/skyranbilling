<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;

use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/users", name="users")
     */
    public function index()
    {
        return $this->render('users/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/inscription", name="registration_users")
     */
    public function registration(Request $request, UserPasswordEncoderInterface $encoder)
    {

        if ($this->isGranted('ROLE_ADMIN')) {


            $user = new User();

            $form = $this->createForm(UserType::class, $user);

            $form->handleRequest($request);

            if ($form->isSubMitted() && $form->isValid()) {

                $hash = $encoder->encodePassword($user, $user->getPassword());

                $user->setPassword($hash);
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($user);
                $manager->flush();
                return $this->redirectToRoute('login_users');
            }

            return $this->render('users/registration.html.twig', [
                'formRegistration'  => $form->createView()
                // 'controller_name' => 'UserController',
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * @Route("/", name="login_users")
     */
    public function login()
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirectToRoute('accueil');
        }
        else
        {

            return $this->render('users/login.html.twig', [
                'controller_name' => 'UsersController',
            ]);
        }
    }

    /**
     * @Route("/deconnexion", name="logout_users")
     */
    public function logout()
    {
        // return $this->render('users/recorverPassword.html.twig', [
        //     'controller_name' => 'UsersController',
        // ]);
    }

    /**
     * @Route("/login/fail", name="login_error")
     */
    public function loginFail()
    {
        return $this->render('users/loginError.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }
    

    /**
     * @Route("/Mot-de-passe-oublie", name="recorver_pwd")
     */
    public function recover()
    {
        return $this->render('users/recorverPassword.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }
}

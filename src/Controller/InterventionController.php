<?php

namespace App\Controller;

use App\Entity\Intervention;
use App\Form\InterventionType;
use App\Repository\InterventionRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class InterventionController extends AbstractController
{

    //------------------------FONCTION POUR AJOUT D'UNE INTERVENTION--------------------------
    /**
     * @Route("/ajout/intervention", name="ajout_intervention")
     */
    public function ajouter(Intervention $intervention = null, Request $requette)
    {
        if ($this->isGranted('ROLE_TECH') or $this->isGranted('ROLE_ADMIN')) {

            $intervention = new Intervention();
            $intervention->setNumeroIntervention($this->genererNumInt());

            $form = $this->createForm(InterventionType::class, $intervention);

            $form->handleRequest($requette);

            if ($form->isSubmitted() && $form->isValid()) {
                $intervention->setDateIntervention(new \DateTime());

                $manager = $this->getDoctrine()->getManager();

                $manager->persist($intervention);

                $manager->flush();

                return $this->redirectToroute('liste_intervention');
            }

            return $this->render('intervention/ajoutIntervention.html.twig', ['formIntervention' => $form->createView()]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }


    /**
     * @Route("intervention/genererNum", name="intervention_number")
     */
    public function genererNumInt(): String
    {
        if ($this->isGranted('ROLE_TECH') or $this->isGranted('ROLE_ADMIN')) {

            $nb = $this->getDoctrine()->getRepository(Intervention::class)->countIntervention();

            $val = (string) $nb + 1;

            $num = ('Int-' . $val);

            return $num;
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
    //------------------------------FONCTION POUR LISTER LES INTERVENTIONS-------------------
    /**
     * @Route("/intervention/liste", name="liste_intervention")
     */
    public function listeIntervention()
    {
        if ($this->isGranted('ROLE_TECH') or $this->isGranted('ROLE_ADMIN')) {

            $repo = $this->getDoctrine()->getRepository(Intervention::class);

            $interventions = $repo->findAll();

            return $this->render('intervention/listeIntervention.html.twig', ['controller_name' => 'ProduitController', 'interventions' => $interventions]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
    //------------------------------FONCTION POUR MODIFIER UNE INTERVENTION----------------------------------
    /**
     * @Route("/modifier/intervention/{id}", name="modifier_intervention")
     */
    public function modifierIntervention(Intervention $intervention, Request $requette)
    {
        if ($this->isGranted('ROLE_TECH') or $this->isGranted('ROLE_ADMIN')) {

            $form = $this->createForm(InterventionType::class, $intervention);

            $form->handleRequest($requette);

            if ($form->isSubmitted() && $form->isValid()) {

                $manager = $this->getDoctrine()->getManager();

                $manager->persist($intervention);

                $manager->flush();

                return $this->redirectToroute('liste_intervention');
            }

            return $this->render('intervention/ajoutIntervention.html.twig', ['formIntervention' => $form->createView()]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }


    //----------------------------------FONCTION POUR SUPPRIMER LES INTERVENTIONS------------------------------
    /**
     * @Route("/suprimer/intervention/{id}", name="supprimer_intervention")
     */
    public function supprimerIntervention(Intervention $intervention)
    {
        if ($this->isGranted('ROLE_TECH') or $this->isGranted('ROLE_ADMIN')) {

            $manager = $this->getDoctrine()->getManager();

            $manager->remove($intervention);

            $manager->flush();

            return $this->redirectToroute('liste_intervention');
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
}

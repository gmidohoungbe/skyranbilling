<?php

namespace App\Controller;

use App\Entity\Facture;
use App\Entity\Paiement;
use App\Form\PaiementType;
use App\Form\PaiementRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PaiementController extends AbstractController
{

    //------------------------------------FONCTION POUR AJOUTER UN PAIEMENT------------------------------
    /**
     * @Route("/paiement/ajout/{id}", name="ajout_paiement")
     */
    public function ajouter(Facture $facture, Request $requette)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $paiement = new Paiement();

            $form = $this->createForm(PaiementType::class, $paiement);

            $form->handleRequest($requette);

            if ($form->isSubmitted() && $form->isValid()) {

                $paiement->setDate(new \DateTime());
                $paiement->setFacture($facture);
                if ($paiement->getMontant() > $facture->getReste()) {
                    # code...
                    // Page d'erreur ici svp
                } else {
                    $facture->setMontantPaye($facture->getMontantPaye() + $paiement->getMontant());
                    $facture->setReste($facture->getReste() - $paiement->getMontant());
                }


                $manager = $this->getDoctrine()->getManager();

                $manager->persist($paiement);
                $manager->flush($paiement);
                $manager->persist($facture);
                $manager->flush($facture);

                return $this->redirectToRoute('liste_paiement');
            }

            return $this->render('paiement/ajoutPaiement.html.twig', [
                'formPaiement' => $form->createView()
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }


    //-----------------------------------FONCTION POUR LISTER LES ELEMENTS----------------------------------------

    /**
     * @Route("/paiement/liste", name="liste_paiement")
     */

    public function lister()
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $repo = $this->getDoctrine()->getRepository(Paiement::class);

            $paiements = $repo->findAll();

            return $this->render('paiement/listePaiement.html.twig', ['paiements' => $paiements]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }


    //----------------------------------FONCTION POUR MODIFIER LES PAIEMENTS--------------------------------
    /**
     * @Route("/paiement/{id}/modifier", name="modifier_paiement")
     */

    public function modifier(Paiement $paiement, Request $requette)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $form = $this->createForm(PaiementType::class, $paiement);

            $form->handleRequest($requette);

            if ($form->isSubmitted() && $form->isValid()) {
                $paiement->setDate(new \DateTime());
                $manager = $this->getDoctrine()->getManager();

                $manager->persist($paiement);

                $manager->flush($paiement);

                return $this->redirectToRoute('liste_paiement');
            }

            return $this->render('paiement/ajoutPaiement.html.twig', [
                'formPaiement' => $form->createView()
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    //----------------------------------FONCTION POUR MODIFIER LES PAIEMENTS--------------------------------
    /**
     * @Route("/paiement/{id}/supprimer", name="supprimer_paiement")
     */

    public function supprimer(Paiement $paiement)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $manager = $this->getDoctrine()->getManager();

            $manager->remove($paiement);

            $manager->flush();

            return $this->redirectToRoute('liste_paiement');
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
}

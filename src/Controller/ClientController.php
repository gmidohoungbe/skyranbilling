<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
// use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ClientRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class ClientController extends AbstractController
{

    /**
     * @Route("/ajout/client", name="ajout_client")
     */

    public function ajouterClient(Request $request)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $client = new Client();

            $form = $this->createForm(ClientType::class, $client);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $client->setCreatedAt(new \DateTime());
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($client);
                $manager->flush();

                return $this->redirectToRoute('liste_client');
            }


            return $this->render('client/ajoutClient.html.twig', [
                'formClient' => $form->createView()
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    //Fonction de modification d'un client
    /**
     * @Route("/modifier/client/{id}", name="modification_client")
     */

    public function modifierClient(Client $client, Request $request, ClientRepository $clientRepository)
    {
        if ($this->isGranted('ROLE_ADMIN')) {


            if (!$client) {
                throw $this->createNotFoundException(
                    "Aucun client ayant l'ID "
                );
            }

            $form = $this->createForm(ClientType::class, $client);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                //$client -> setCreatedAt(now); //Prévoir un champ lastUpdated
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($client);
                $manager->flush(); // Se passer de cette ligne pour voir ce que ça donne

                $this->addFlash('message', 'Votre modification a été bien effectuée.');
                return $this->redirectToRoute('liste_client');
            }

            return $this->render('client/ajoutClient.html.twig', [
                'formClient' => $form->createView(),
                'editMode' => $client->getId() !== null,

            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    // Fonction de suppression d'un client : ON DOIT AJOUTER DES CONTROLES SUR LE COMPTE UTILSATEUR, TOUT LE MONDE NE PEUT PAS AJOUTER DE CLIENT!

    /**
     * @Route("/supprimer/client/{id}", name="suppression_client")
     */

    public function supprimerClient($id, Request $request, ClientRepository $clientRepository)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            $client = $clientRepository
                ->find($id);

            if (!$client) {
                throw $this->createNotFoundException(
                    "Aucun client ayant l'ID " . $id
                );
            } else {
                $manager = $this->getDoctrine()->getManager();
                $manager->remove($client);
                $manager->flush();
            }

            return $this->redirectToRoute('liste_client');
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    //Fonctionn qui affiche la liste des clients

    /**
     * @Route("/client/listeClients", name="liste_client")
     */

    public function listerClient(ClientRepository $clientRepository)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $clients = $clientRepository
                ->findAll();

            return $this->render('client/listeClient.html.twig', [
                'controller_name' => 'ClientController',
                'clients' => $clients
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
}

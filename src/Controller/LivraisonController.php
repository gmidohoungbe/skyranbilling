<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

use App\Entity\Livraison;
use App\Form\LivraisonType;
use App\Form\LivraisonRepository;

class LivraisonController extends AbstractController
{

//------------------------------------FONCTION POUR AJOUTER UNe LIVRAISON------------------------------
    /**
     * @Route("/livraison/ajout", name="ajout_livraison")
     */
    public function ajouter(Livraison $livraison = null, Request $requette)
    {
    	$livraison = new Livraison();

    	$form = $this->createForm(LivraisonType::class, $livraison);

    	$form->handleRequest($requette);

    	if($form->isSubmitted() && $form->isValid())
    	{	
    		$livraison->setDateLivraison(new \ DateTime());

    		$manager=$this->getDoctrine()->getManager();

    		$manager->persist($livraison);

    		$manager->flush($livraison);

    		return $this->redirectToRoute('liste_livraison');
    	}

        return $this->render('livraison/ajoutLivraison.html.twig', [
            'formLivraison'=>$form->createView()
        ]);
    }

    /**
     * @Route("livraison/genererNum", name="livraison_number")
     */
    public function genererNumLiv():String
    {

        // $nb = $livraisons->countLivraison();
        $x=$this->getDoctrine()->getRepository(Livraison::class);
        $nb=$x->countLivraison();

        $num=('Liv-'.$nb);

        return $num;
        
    }

 //-----------------------------------FONCTION POUR LISTER LES ELEMENTS-----------------------------

    /**
     * @Route("/livraison/liste", name="liste_livraison")
     */

    public function lister()
    {
    	$repo=$this->getDoctrine()->getRepository(Livraison::class);

    	$livraisons=$repo->findAll();

    	return $this->render('livraison/listeLivraison.html.twig', ['livraisons'=>$livraisons]);
    }


//----------------------------------FONCTION POUR MODIFIER LES LIVRAISONS--------------------------------
    /**
     * @Route("/livraison/{id}/modifier", name="modifier_livraison")
     */

    public function modifier(Livraison $livraison, Request $requette)
    {
    	$form=$this->createForm(LivraisonType::class, $livraison);

    	$form->handleRequest($requette);

    	if($form->isSubmitted() && $form->isValid())
    	{
    		$manager=$this->getDoctrine()->getManager();

    		$manager->persist($livraison);

    		$manager->flush($livraison);

    		return $this->redirectToRoute('liste_livraison');
    	}

    	return $this->render('livraison/ajoutLivraison.html.twig', [
            'formLivraison'=>$form->createView()
        ]);
    }

    //----------------------------------FONCTION POUR MODIFIER LES LIVRAISONS------------------------
    /**
     * @Route("/livraison/{id}/supprimer", name="supprimer_livraison")
     */

    public function supprimer(Livraison $livraison)
    {
    	$manager= $this->getDoctrine()->getManager();

    	$manager->remove($livraison);

    	$manager->flush();

    	return $this->redirectToRoute('liste_livraison');
    }
}

<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditUserType;
use App\Repository\UsersRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;



class AdminController extends AbstractController
{
    /**
     * @Route("/admin1", name="admin1")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }


    /**
     * Liste des utilisateurs

     * @Route("/admin/utilisateur", name="utilisateurs")
     */
    public function usersList(UsersRepository $users)
    {

        if ($this->isGranted('ROLE_ADMIN')) {

            return $this->render('admin/users.html.twig', [
                'users' => $users->findAll(),
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * Modifier utilisateurs

     * @Route("/utilisateur/modifier/{id} ", name="modifier_users")
     */
    public function editUser(User $users, Request $request)
    {

        if ( $this->isGranted('ROLE_ADMIN')) {

            $form = $this->createForm(EditUserType::class, $users);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->persist($users);
                $entityManager->flush();

                $this->addFlash('message', 'Utilisateur modifié avec succès');
                
                return $this->redirectToRoute('utilisateurs');
            }

            return $this->render('admin/edituser.html.twig', [
                'userForm' => $form->createView(),
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
}

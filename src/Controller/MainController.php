<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/accueil", 
     *  options = {"expose" = true}, 
     * name="accueil")
     */
    public function index()
    {
        //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }
}

<?php

namespace App\Controller;


use App\Entity\Produit;
use App\Form\ProduitType;
use App\Repository\ProduitRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class ProduitController extends AbstractController
{
    //---------------------------------FONCTION POUR AJOUTER LES ELEMENTS-------------------------------------
    /**
     * @Route("/ajout/produit", name="ajout_produit")
     */
    public function ajouter(Produit $produit = null, Request $requette)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            $produit = new Produit();

            $form = $this->createForm(ProduitType::class, $produit);

            $form->handleRequest($requette);

            if ($form->isSubmitted() && $form->isValid()) {

                $manager = $this->getDoctrine()->getManager();
                $manager->persist($produit);

                $manager->flush();

                return $this->redirectToRoute('liste_produit');
            }

            return $this->render('produit/ajoutProduit.html.twig', [
                'formProduit' => $form->createView()
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }


    //-----------------------------------FONCTION POUR LISTER LES ELEMENTS-------------------------------------------

    /**
     * @Route("/listeProduit", name="liste_produit")
     */
    public function liste()
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $repo = $this->getDoctrine()->getRepository(Produit::class);

            $produits = $repo->findAll();

            return $this->render('produit/listeProduit.html.twig', [
                'controller_name' => 'ProduitController', 'produits' => $produits
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }



    //-------------------------------FONCTION POUR MODIFIER LES ELEMENTS-------------------------------------------

    /**
     * @Route("/modifier/produit/{id}", name="modifier_produit")
     */
    public function modifierProduit(Produit $produit = null, Request $requette)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $form = $this->createForm(ProduitType::class, $produit);

            $form->handleRequest($requette);

            if ($form->isSubmitted() && $form->isValid()) {

                $manager = $this->getDoctrine()->getManager();
                $manager->persist($produit);
                $manager->flush();

                return $this->redirectToRoute('liste_produit');
            }

            return $this->render('produit/ajoutProduit.html.twig', [
                'formProduit' => $form->createView(), 'editMode' => $produit->getId() !== null
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    //-------------------------------FONCTION POUR SUPPRIMER LES  ELEMENTS-------------------------------------------

    /**
     * @Route("/supprimer/produit/{id}", name="supprimer_produit")
     *
     *
     */
    public function supprimerProduit(Produit $produit)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            $manager = $this->getDoctrine()->getManager();
            $manager->remove($produit);
            $manager->flush();
            return $this->redirectToRoute('liste_produit');
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
}

<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Produit;
use App\Entity\Proforma;
use App\Form\ProformaType;
use App\Service\PanierService;
use App\Entity\DetailsProforma;
use App\Form\Produit\ProduitType;
use App\Repository\ClientRepository;
use App\Repository\ProduitRepository;
use App\Repository\ProformaRepository;
use Symfony\Component\Serializer\Serializer;
use App\Repository\DetailsProformaRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
//use App\Service\SerializerService;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProformaController extends AbstractController
{
    /**
     * @Route("/proforma", name="proforma")
     */
    public function index()
    {
        return $this->render('proforma/index.html.twig', [
            'controller_name' => 'ProformaController',
        ]);
    }

    //----------------------FONCTION POUR AFFICHER LA LISTE DES PRODUITS--------------------------

    /**
     * @Route("prof/ajoutProf", options ={ "expose" = true},name="ajout_prof")
     */
    public function ajouterProf(Proforma $prof = null, ProduitRepository $repo)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            $produits = $repo->findInStock();

            $prof = new Proforma();

            $form = $this->createForm(ProformaType::class, $prof);

            return $this->render('proforma/ajoutProforma.html.twig', [
                'produits' => $produits,
                'formProforma' => $form->createView()
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }


    //-----------------------FONCTION POUR AJOUTER AU PANIER--------------------------------------

    /**
     * @Route("/prof/panier/{id}",options ={ "expose" = true}, name="ajout_panier")
     * 
     */
    public function ajoutPanier($id, PanierService $monpanier)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            $monpanier->ajouter($id);
            return $this->json([
                'message' => "Produit ajouté au panier"
            ], 200);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    //---------------------------------------FONCTION POUR VOIR LE PANIER--------------------------

    /**
     * @Route("/prof/voir/panier",options ={ "expose" = true}, name="voir_panier")
     * 
     */

    public function voirPanier(PanierService $monpanier): Response
    {

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);
        $listePanier = $monpanier->obtenirPanier();

        $jsonContent = $serializer->serialize($listePanier, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['client', 'facture', 'detailsCommandes', 'livraisons', 'detailsVentes', 'detailsProformas']]);

        return $this->json([
            $jsonContent
        ], 200);
    }


    //---------------------------------FONCTION POUR MODIFIER LE PANIER-----------------------------

    /**
     * @Route("/prof/modifier/panier/{id}/{qte}",options ={ "expose" = true}, name="modifier_panier")
     * 
     */
    public function modifierPanier($id, $qte, PanierService $monpanier)
    {
        $monpanier->modifier($id, $qte);
        $listePanier = $monpanier->obtenirProduitPanier($id);
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($listePanier, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['client', 'facture', 'detailsCommandes', 'livraisons', 'detailsVentes', 'detailsProformas']]);
        return $this->json([
            $jsonContent
        ], 200);
    }


    //--------------------------------------FONCTION POUR RETIRER DU PANIER--------------------------

    /**
     * @Route("/prof/retirer/panier/{id}", name="retirer_panier")
     */
    public function supprimerPanier($id, PanierService $monpanier): Response
    {

        $monpanier->supprimer($id);
        return $this->json(['message' => "Produit supprimé du panier"], 200);
    }


    //--------------------------------------ANNULER LA VENTE--------------------------------------
    /**
     * @Route("/prof/annuler",options ={ "expose" = true}, name="annuler_prof")
     */
    public function annulerProf(PanierService $monpanier)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $monpanier->viderPanier();
            return $this->json(['message' => "Opération annulée"], 200);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }


    //---------------------------------------VALIDER LA VENTE---------------------------------------
    /**
     * @Route("/prof/valider/{client}/{desc}/{ht}/{ttc}",options ={ "expose" = true}, name="valider_prof")
     *
     */
    public function validerProf($client, $desc, $ht, $ttc, ClientRepository $clients, PanierService $monpanier, ProduitRepository $produits)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            // Création de la vente
            $client = $clients->findOneById($client);
            $prof = new Proforma();
            $prof->setClient($client);
            $prof->setDescription($desc);
            $prof->setTotalHT($ht);
            $prof->setTotalTTC($ttc);
            $prof->setDatePro(new \DateTime());
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($prof);
            $manager->flush();

            // Création des détails de la vente
            $listePanier = $monpanier->obtenirPanier();

            foreach ($listePanier as $idprod => $item) {

                $detProf = new DetailsProforma();

                $detProf->setProforma($prof);
                $prod = $produits->findOneById($item['produit']->getId());
                $detProf->setProduit($prod);
                $detProf->setQuantite($item['quantite']);
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($detProf);
                $manager->flush();
            }
            $monpanier->viderPanier();

            return $this->redirectToRoute('liste_prof');
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * @Route("/prof/listeProf",options = {"expose" = true}, name="liste_prof")
     *
     */
    public function listerProf(ProformaRepository $profsRepository)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $proff = $profsRepository->findAll();

            return $this->render('proforma/listeProforma.html.twig', [
                'profs' => $proff
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * @Route("/prof/detailsProf/{id}", name="details_prof")
     *
     */
    public function detailsProf($id, DetailsProformaRepository $detprofsRepository)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $detprofs = $detprofsRepository->findAll();
            // $ventes = $detventesRepository->createQueryBuilder('DetailsVente')
            //     ->select('DetailsVente.quantite','DetailsVente.quantite','DetailsVente.produit')
            //     ->where('DetailsVente.vente' = $id)
            //     ->getQuery();
            // $detventes=$ventes->getOneOrNullResult();
            return $this->render('proforma/detailsProforma.html.twig', [
                'detprofs' => $detprofs, 'identifiant' => $id
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
    /**
     * @Route("/prof/voirProf/{id}", name="vue_prof")
     *
     */
    public function voirProf($id, ProformaRepository $profsRepository)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {


            $proforma = $profsRepository->findOneById($id);

            return $this->render('proforma/vueProforma.html.twig', [
                'proforma' => $proforma
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * @Route("prof/modifierProf", name="modifier_prof")
     */
    public function modifierProf(ProformaRepository $profsRepository)
    {

        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {

            $profs = $profsRepository
                ->findAll();
                
            return $this->render('vente/listeProforma.html.twig', [
                'profs' => $profs
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
    /**
     * @Route("prof/supprimerProf", name="supprimer_prof")
     */
    public function supprimerProf(ProformaRepository $profsRepository)
    {
        if ($this->isGranted('ROLE_COM') or $this->isGranted('ROLE_ADMIN')) {
            $profs = $profsRepository
                ->findAll();
            return $this->render('proforma/listeProforma.html.twig', [
                'profs' => $profs
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
}

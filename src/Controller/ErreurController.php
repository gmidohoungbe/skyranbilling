<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ErreurController extends AbstractController
{
    /**
     * @Route("/erreur", name="erreur")
     */
    public function index()
    {
        return $this->render('erreur/index.html.twig', [
            'controller_name' => 'ErreurController',
        ]);
    }

    /**
     * @Route("/erreur/404", name="erreur_404")
     */
    public function error404()
    {
        return $this->render('erreur/404.html.twig', [
            'controller_name' => 'ErreurController',
        ]);
    }

    /**
     * @Route("/erreur/500", name="erreur_500")
     */
    public function error500()
    {
        return $this->render('erreur/500.html.twig', [
            'controller_name' => 'ErreurController',
        ]);
    }
}

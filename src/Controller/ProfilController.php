<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditProfilType;
use App\Repository\UsersRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfilController extends AbstractController
{
    /**
     * @Route("/profil", name="profil")
     */
    public function index()
    {
        if ($this->isGranted('ROLE_TECH') or $this->isGranted('ROLE_ADMIN')) {

            return $this->render('profil/index.html.twig', [
                'controller_name' => 'ProfilController',
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }


    /**
     * Liste des utilisateurs

     * @Route("/profil/utilisateur", name="profil_user")
     */
    public function usersList(UsersRepository $users)
    {
        if ($this->isGranted('ROLE_TECH') or $this->isGranted('ROLE_ADMIN')) {

            return $this->render('profil/myProfil.html.twig', [

                'users' => $this->getUser(),

            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }

    /**
     * Modifier utilisateurs

     * @Route("/profile/modif/password/{id} ", name="modifier_profil")
     */
    public function editUser(User $users, Request $request, UserPasswordEncoderInterface $encoder)
    {
        if ($this->isGranted('ROLE_TECH') or $this->isGranted('ROLE_ADMIN')) {

            $form = $this->createForm(EditProfilType::class, $users);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($users);
                $entityManager->flush();
                $hash = $encoder->encodePassword($users, $users->getPassword());

                $users->setPassword($hash);
                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->persist($users);
                $entityManager->flush();


                //  $session = $this->get('session');
                //  $session->clear();
                // $session->invalidate();
                //var_dump($session);
                //var_dump($request->getSession());

                // return $this->redirectToRoute('logout_users'); 
                $this->addFlash('success', 'Mot de passe modifié avec succès');
                return $this->redirectToRoute('profil_user');
            }

            return $this->render('profil/editProfil.html.twig', [
                'profilForm' => $form->createView(),
            ]);
        } else {
            throw new AccessDeniedException('Accès limité. Veuillez conctatez votre administrateur.');
        }
    }
}
